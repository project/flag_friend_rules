<?php
// $Id$

/**
 * @file
 * Hooks and callback functions for rules.module integration.
 * TODO: Add various actions?
 */

/**
 * Implementation of hook_rules_event_info().
 * TODO: Pass user message as argument
 */
function flag_friend_rules_rules_event_info() {
  $arguments = array(
    'friend_account' => array(
      'type' => 'user',
      'label' => t('Friend account'),
      'description' => t('User being flagged.')
    ),
    'account' => array(
      'type' => 'user',
      'label' => t('Account'),
      'description' => t('User doing the flagging.')
    ),  
  );  
  return array(
    'rules_event_flag_friend_rules_request' => array(
      'label' => t('Friend request sent'),
      'module' => 'Flag Friend Rules',
      'arguments' => $arguments,
    ),
    'rules_event_flag_friend_rules_approve' => array(
      'label' => t('Friend request approved'),
      'module' => 'Flag Friend Rules',
      'arguments' => $arguments,
    ),    
    'rules_event_flag_friend_rules_deny' => array(
      'label' => t('Friend request denied'),
      'module' => 'Flag Friend Rules',
      'arguments' => $arguments,
    ),
    'rules_event_flag_friend_rules_remove' => array(
      'label' => t('Friend removed'),
      'module' => 'Flag Friend Rules',
      'arguments' => $arguments,
    ),    
  );
}